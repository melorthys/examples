#!/bin/bash
set -e

DOMAIN=allgames.kagan.dev

PACKAGE="bind-utils"
if [ -z "$(sudo yum list installed | grep $PACKAGE)" ]; then
  echo "Installing $PACKAGE package."
  sudo yum -y install $PACKAGE
fi

PACKAGE="curl"
if [ -z "$(sudo yum list installed | grep $PACKAGE)" ]; then
  echo "Installing $PACKAGE package."
  sudo yum -y install $PACKAGE
fi

DOMAIN_IP=$(nslookup $DOMAIN | grep Address | awk 'NR>1{print $2}')
PUBLIC_IP=$(curl -s ifconfig.me)

if [ $DOMAIN_IP == $PUBLIC_IP ]; then
  echo "$DOMAIN IP address is correct."
else
  echo "$DOMAIN IP address is NOT correct."
fi

mkdir -p /var/www/$DOMAIN

cat << EOF | sudo tee /etc/nginx/conf.d/$DOMAIN.conf 1>/dev/null
server {
    listen 80;
    listen [::]:80;

    server_name $DOMAIN;

    root   /var/www/$DOMAIN;
    index  index.html;
}
EOF

sudo nginx -t

if test $(echo $?) -eq 0; then
  echo "Nginx configuration is OK."
else
  echo "ERROR in nginx configuration."
fi

sudo certbot certonly --nginx --non-interactive --agree-tos -m kaganmert@hotmail.com.tr -d $DOMAIN

cat << EOF | sudo tee /etc/nginx/conf.d/$DOMAIN.conf 1>/dev/null
server {
    listen 80;
    listen [::]:80;
    server_name $DOMAIN;
    return 301 https://$DOMAIN$request_uri;
}

server {
    server_name $DOMAIN;
    root /var/www/$DOMAIN;

    listen [::]:443 ssl;
    listen 443 ssl;
    ssl_certificate /etc/letsencrypt/live/$DOMAIN/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/$DOMAIN/privkey.pem;
    include /etc/letsencrypt/options-ssl-nginx.conf;
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem;
}
EOF

sudo nginx -t

if test $(echo $?) -eq 0; then
  echo "Nginx configuration is OK. Restarting nginx server."
  sudo systemctl restart nginx
else
  echo "ERROR in nginx configuration."
fi

echo "$DOMAIN cert is noicely done mate."
